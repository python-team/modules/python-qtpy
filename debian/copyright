Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qtpy
Upstream-Contact: spyder.python@gmail.com
Source: https://github.com/spyder-ide/qtpy

Files: *
Copyright: 2011- QtPy contributors and others (see AUTHORS.md)
  2009- The Spyder Development Team
License: Expat
Comment: This is from LICENSE.txt and PKG-INFO/README.md

Files: debian/*
Copyright: 2016-2017 Ghislain Antony Vaillant
  2019- QtPy Debian Developers (see changelog.Debian.gz)
License: Expat

Files:
 qtpy/QtCore.py
 qtpy/QtGui.py
 qtpy/QtNetwork.py
 qtpy/QtTest.py
 qtpy/QtWebEngine.py
 qtpy/QtWebEngineWidgets.py
 qtpy/QtWidgets.py
 qtpy/__init__.py
Copyright: 2014-2015 Colin Duquesnoy
 2009- The Spyder Development Team
License: Expat

Files: qtpy/QtDesigner.py
Copyright: 2014-2015 Colin Duquesnoy
License: Expat

Files: qtpy/QtPositioning.py
Copyright: 2020 Antonio Valentino
License: Expat

Files: qtpy/QtSerialPort.py
Copyright: 2020 Marcin Stano
 2009- The Spyder Development Team
License: Expat

Files: qtpy/uic.py
Copyright: 2015, Chris Beaumont and Thomas Robitaille
 2011 Sebastian Wiesner <lunaryorn@gmail.com>
 Charl Botha <cpbotha@vxlabs.com>
 2009- The Spyder Development Team
License: BSD-3-clause and Expat
 The file states the following:
 .
 In PySide, loadUi does not exist, so we define it using QUiLoader, and
 then make sure we expose that function. This is adapted from qt-helpers
 which was released under a 3-clause BSD license:
 qt-helpers - a common front-end to various Qt modules
 .
 Copyright (c) 2015, Chris Beaumont and Thomas Robitaille
 .
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.
  * Neither the name of the Glue project nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 Which itself was based on the solution at
 .
 https://gist.github.com/cpbotha/1b42a20c8f3eb9bb7cb8
 .
 which was released under the MIT license:
 .
 Copyright (c) 2011 Sebastian Wiesner <lunaryorn@gmail.com>
 Modifications by Charl Botha <cpbotha@vxlabs.com>
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.


Files: qtpy/enums_compat.py
Copyright: 2009- The Spyder Development Team
  2012- University of North Carolina at Chapel Hill
        Luke Campagnola    ('luke.campagnola@%s.com' % 'gmail')
        Ogi Moore          ('ognyan.moore@%s.com' % 'gmail')
        KIU Shueng Chuan   ('nixchuan@%s.com' % 'gmail')
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
